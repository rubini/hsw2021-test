#include "povacca.h"

int pv_twice(struct pv_stack *s)
{
	s->vals[s->sp - 1] *= 2;
	return 0;
}
pv_operand(twice, "twice", 1, pv_twice);
