/* An RPL calculator ('na vaccata) */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "povacca.h"

static int pv_do_arg(struct pv_stack *s, char *arg)
{
	int i; char c;
	struct pv_oper *op;

	for (op = pv_first; op < pv_last; op++)
		if (!strcmp(arg, op->name))
			break;
	if (op < pv_last) {
		if (s->sp < op->sp_min)
			return -1;
		return op->func(s);
	}
	if (sscanf(arg, "%i%c", &i, &c) != 1)
		return -2;
	s->vals[s->sp++] = i;
	return 0;
}

int main(int argc, char ** argv)
{
	int i, ret;
	static struct pv_stack stack;

	for (i = 1; i < argc; i++)
		if ( (ret = pv_do_arg(&stack, argv[i])) < 0)
			exit(ret);
	for (i = 0; i < stack.sp; i++)
		printf("%2i: %i\n", i, stack.vals[i]);
	exit(0);
}
