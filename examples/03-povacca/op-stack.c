#include "povacca.h"

int pv_pop(struct pv_stack *s)
{
	s->sp--;
	return 0;
}

pv_operand(pop, "pop", 1, pv_pop);


int pv_dup(struct pv_stack *s)
{
	s->vals[s->sp] = s->vals[s->sp - 1];
	s->sp++;
	return 0;
}

pv_operand(dup, "dup", 1, pv_dup);

int pv_exch(struct pv_stack *s)
{
	int val = s->vals[s->sp - 1];
	s->vals[s->sp - 1] = s->vals[s->sp - 2];
	s->vals[s->sp - 2] = val;
	return 0;
}

pv_operand(exch, "exch", 2, pv_exch);
