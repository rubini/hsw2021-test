#ifndef __POVACCA_H__
#define __POVACCA_H__

struct pv_stack {
	int vals[32];
	int sp;
};

struct pv_oper {
	char *name;
	int sp_min;
	int (*func)(struct pv_stack *s);
};

#define pv_operand(strname, opname, min, f)	  \
    struct pv_oper                        \
    __attribute__((__section__(".oper"), __aligned__(1)))	\
    __oper_##strname = { \
	    .name = opname, .sp_min = min, .func = f \
    }

extern struct pv_oper pv_first[], pv_last[];

#endif /* __POVACCA_H__ */
