#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
	int c;

	if (argc != 1)
		exit(1);

	while ( (c = getchar()) != EOF) {
		if (c >= 'a' && c <= 'z') {
			c += 13;
			if (c > 'z')
				c -= 26;
		}
		if (c >= 'A' && c <= 'Z') {
			c += 13;
			if (c > 'Z')
				c -= 26;
		}
		putchar(c);
	}
	return 0;
}
