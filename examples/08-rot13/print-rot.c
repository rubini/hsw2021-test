#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
	int c, i;

	if (argc != 1)
		exit(1);

	for (i = 0; i < 256; i++) {
		c = i;
		if (c >= 'a' && c <= 'z') {
			c += 13;
			if (c > 'z')
				c -= 26;
		}
		if (c >= 'A' && c <= 'Z') {
			c += 13;
			if (c > 'Z')
				c -= 26;
		}
		printf("0x%02x,%c", c, " \n"[(i & 0x7) == 0x7]);
	}
	return 0;
}
