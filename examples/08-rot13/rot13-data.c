#include <stdio.h>
#include <stdlib.h>

const unsigned char convert[] = {
#include "rot13.h"
};

int main(int argc, char **argv)
{
	int c;

	if (argc != 1)
		exit(1);

	while ( (c = getchar()) != EOF)
		putchar(convert[c]);
	return 0;
}
