#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Don't sort but allocate like for qsort or for tree */

#define SLEN 16
#define NSAMPLE 1024 /* initial count for array size */

/* for the tree-line allocation */
struct node {
	char s[SLEN];
	struct node *left;
	struct node *right;
};


int main(int argc, char **argv)
{
	char line[SLEN];
	int i = 0, j;
	int nsample = NSAMPLE;
	char (*samples)[SLEN]; /* This is realloced by powers of 2 */
	struct node *node, *head = NULL;

	if (argc > 1) { /* pass "a" or whatever */
		samples = malloc(SLEN * nsample);
		if (!samples)
			exit(1);

		/* read data */
		while (fgets(line, SLEN, stdin)) {
			memcpy(samples[i++], line, SLEN);
			if (i >= nsample) {
				nsample *= 2;
				samples = realloc(samples, SLEN * nsample);
				if (!samples)
					exit(2);
			}
		}
		/* write it back */
		for (j = 0; j < i; j++)
			printf("%s", samples[j]);
		return 0;
	}

	/* else, allocate like for the tree, but no tree is done */
	while (fgets(line, SLEN, stdin)) {
		node = calloc(1, sizeof(*node));
		strcpy(node->s, line);
		node->left = head;
		head = node;
	}

	/* print it back */
	for (node = head; node; node = node->left)
		printf("%s", node->s);

	return 0;
}
