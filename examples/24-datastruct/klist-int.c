/* Alessandro Rubini 2010, public domain */
#include <stdio.h>
#include <stdlib.h>
#include "linux_list.h" /* copied from linux/list.h in 2.6.31 */

struct int_item {
	int value;
	struct list_head list;
};

/* This program reads integers from stdin and prints it reversed */
int main(int argc, char **argv)
{
	struct list_head head;
	struct list_head *list;
	struct int_item *item;
	char line[16];

	INIT_LIST_HEAD(&head);

	/* read data */
	while (fgets(line, 16, stdin)) {
		item = malloc(sizeof(*item));
		item->value = atoi(line);
		list_add(&item->list, &head);
	}

	/* write it back */
	list_for_each(list, &head) {
		item = container_of(list, struct int_item, list);
		printf("%12i\n", item->value);
	}

	/* free it all */
	while (!list_empty(&head)) {
		list = head.next;
		item = container_of(list, struct int_item, list);
		list_del(list);
		free(item);
	}


	return 0;
}

