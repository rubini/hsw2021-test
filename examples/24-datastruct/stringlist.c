/* Alessandro Rubini 2010, public domain */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct str_item {
	char str[16];
	struct str_item *next;
};

#define list_insert(h, new) \
	({(new)->next = (h); (h) = (new);})

#define list_extract(h) \
	({struct str_item *res = (h); if (h) (h) = (h)->next; res;})

/* This program reads integers from stdin and prints it reversed */
int main(int argc, char **argv)
{
	struct str_item *head = NULL;
	struct str_item *item;
	char line[16];

	/* read data */
	while (fgets(line, 16, stdin)) {
		item = malloc(sizeof(*item));
		memcpy(item->str, line, sizeof(item->str));
		list_insert(head, item);
	}

	/* write it back */
	while ( (item = list_extract(head)) ) {
		printf("%s", item->str);
		free(item);
	}

	return 0;
}
