#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SLEN 16

struct node {
	char s[SLEN];
	struct node *left;
	struct node *right;
};

int tt_insert(struct node *tree, char *s)
{
	struct node *new, **nextp;
	if (strcmp(tree->s, s) > 0) {
		if (tree->left)
			return tt_insert(tree->left, s);
		else
			nextp = &tree->left;
	} else {
		if (tree->right)
			return tt_insert(tree->right, s);
		else
			nextp = &tree->right;
	}
	new = calloc(1, sizeof(*new));
	if (!new)
		return -1;
	strcpy(new->s, s);
	*nextp = new;
	return 0;
}

void tt_print(struct node *tree)
{
	if (tree->left)
		tt_print(tree->left);
	printf("%s", tree->s);
	if (tree->right)
		tt_print(tree->right);
}

int main(int argc, char **argv)
{
	char line[SLEN];
	struct node *root = NULL;

	memset(&root, 0, sizeof(root));

	/* first line */
	if (fgets(line, SLEN, stdin)) {
		root = calloc(1, sizeof(*root));
		strcpy(root->s, line);
	}
	/* read other lines and insert them */
	while (fgets(line, SLEN, stdin))
		if (tt_insert(root, line))
			exit(1);

	/* print the tree back */
	if (root)
		tt_print(root);

	/* do not free the array */

	return 0;
}
