/* Alessandro Rubini 2010, public domain */
#include <stdio.h>
#include <stdlib.h>
#include "generic_list.h"

/* This program reads integers from stdin and prints it reversed */
int main(int argc, char **argv)
{
	struct generic_list *head = NULL;
	struct generic_list *item;
	char line[16];

	/* read data */
	while (fgets(line, 16, stdin)) {
		item = malloc(sizeof(*item));
		item->payload = malloc(sizeof(int));
		*(int *)(item->payload) = atoi(line);
		list_insert(head, item);
	}

	/* write it back */
	while ( (item = list_extract(head)) ) {
		printf("%12i\n", *(int *)(item->payload));
		free(item->payload);
		free(item);
	}

	return 0;
}
