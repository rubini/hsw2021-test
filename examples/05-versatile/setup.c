#include <stdint.h>
#include "hw.h"

/* called by assembly code before main */
void setup(void)
{
	uint32_t timerconf =
		TIMER_CTRL_32BIT | TIMER_CTRL_DIV1 | TIMER_CTRL_PERIODIC;

	/* First configure, but keep it disabled */
	regs[REG_TIMER_CTRL] = timerconf;

	/* counts down to zero, so start at the maximum */
	regs[REG_TIMER_LOAD] = ~0;
	regs[REG_TIMER_BGLOAD] = ~0;

	/* Finally enable it */
	regs[REG_TIMER_CTRL] = timerconf | TIMER_CTRL_ENABLE;
	return;
}
