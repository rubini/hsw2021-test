#include <io.h>
#include <time.h>

extern void uart_init(void);
extern void pll_init(void);
extern void generic_udelay_init(void);

void setup(void)
{
	regs[REG_AHBCLKCTRL] |= REG_AHBCLKCTRL_CT32B1;

	/* enable timer 1, and count at HZ Hz */
	regs[REG_TMR32B1TCR] = 1;
	regs[REG_TMR32B1PR] = (CPU_FREQ / HZ) - 1;

	/* and setup the serial port and pll */
	uart_init();
	pll_init();
	generic_udelay_init();
}
