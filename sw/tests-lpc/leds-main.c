#include <time.h>
#include <io.h>

#define LED1 GPIO_NR(0,  7)
#define LED2 GPIO_NR(1, 28)
#define LED3 GPIO_NR(1, 31)

struct action {
	int gpio;
	int value;
	int delay;
};

struct action actions[] = {
	{LED1, 1, 500*1000},
	{LED1, 0, 500*1000},
	{LED2, 1, 500*1000},
	{LED2, 0, 500*1000},
	{LED3, 1,1000*1000},
	{LED3, 0,1000*1000},
};

void main(void)
{
	unsigned long prev;
	int i;

	printf("This is file" __FILE__ "\n"
	       "Commit %s\n", __GITC__);


	for (i = 0; i < ARRAY_SIZE(actions); i++)
		gpio_dir_af(actions[i].gpio, GPIO_DIR_OUT, 0, GPIO_AF_GPIO);

	prev = jiffies;
	while (1) {
		for (i = 0; i < ARRAY_SIZE(actions); i++) {
			gpio_set(actions[i].gpio, actions[i].value);
			udelay(actions[i].delay);
			printf("jiffies now %10li (delta %6li)\n",
			       jiffies, jiffies - prev);
			prev = jiffies;
		}
	}
}
