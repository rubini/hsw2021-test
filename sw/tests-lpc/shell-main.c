#include <io.h>
#include <irq.h>
#include <time.h>
#include <task.h>
#include <string.h>
#include <command.h>
#include <assert.h>
#include <errno.h>

/* Panic leds: yellow is clock, green is data */
struct panic_ledinfo panic_ledinfo = {
	.gpio_clock = GPIO_NR(0, 7),
	.gpio_data = GPIO_NR(1, 28),
};

static int command_go(char **reply, int argc, char **argv)
{
	int addr;
	char c;
	void (*f)(void);

	if (sscanf(argv[1], "%x%c", &addr, &c) != 1)
		return -EINVAL;
	f = (void *)addr;
	printf("going to %p\n", f);
	f();
	return 0;
}

/* assert <true-false> <ledvalue> */
static int command_assert(char **reply, int argc, char **argv)
{
	int cond, ledval;;
	char c;

	if (sscanf(argv[1], "%x%c", &cond, &c) != 1)
		return -EINVAL;
	if (sscanf(argv[2], "%x%c", &ledval, &c) != 1)
		return -EINVAL;
	assert(cond, ledval, "condition failed (ledval %s)\n", argv[2]);
	return 0;
}

/* sleep <seconds> [<milliseconds>] */
static int command_sleep(char **reply, int argc, char **argv)
{
	unsigned long j;
	int sec, msec = 0;
	char c;

	if (sscanf(argv[1], "%i%c", &sec, &c) != 1)
		return -EINVAL;
	if (argc == 3) {
		if (sscanf(argv[2], "%i%c", &msec, &c) != 1)
			return -EINVAL;
	}
	msec = sec * 1000 + msec;
	j = jiffies + (HZ * msec / 1000);
	while (time_before(jiffies, j))
		;
	return 0;
}

/* prio <0..255> */
static int command_prio(char **reply, int argc, char **argv)
{
	extern struct task t_shell;
	int prio;
	char c;

	if (sscanf(argv[1], "%i%c", &prio, &c) != 1)
		return -EINVAL;
	if (prio < 0 || prio > 255)
		return -EINVAL;
	t_shell.prio = prio;
	return 0;
}

static struct command shell_commands[] = {
	COMMAND_R,
	COMMAND_W,
	COMMAND_GPIO,
	COMMAND_HELP,
	{"go",    command_go,      2, 2},
	{"assert",command_assert,  3, 3},
	{"sleep", command_sleep,   2, 3},
	{"prio",  command_prio ,   2, 2},
	{}
};

/*
 * We use an interrupt-activated task for the leds. We use TMR16B0.
 * The task is running the leds, with PWM on the orange one.
 * The associated data structure is used to represent led status.
 */
struct shell_led_cfg {
	int pwm_high; /* how many periods are high  */
	int pwm_total; /* how many periods total */
	int jiffies_each; /* how many jiffies each led */
	int current_led; /* 0..2 */
};

static struct shell_led_cfg shell_led_cfg = {
	.pwm_high = 4,
	.pwm_total = 100,
	.jiffies_each = HZ,
};

static int shell_led_gpio[] = {
	GPIO_NR(1, 31), /* orange (the one to pwm-down in brightness) */
	GPIO_NR(1, 28),
	GPIO_NR(0, 7),
};

static int shell_led_init(void *arg, struct task *t)
{
	int i;

	for (i = 0; i < ARRAY_SIZE(shell_led_gpio); i++)
		gpio_dir_af(shell_led_gpio[i], GPIO_DIR_OUT, 0, GPIO_AF_GPIO);

	/* Prepare the timer for interrupt. Same code as ../tdc-irq-main.c */
	regs[REG_AHBCLKCTRL] |= REG_AHBCLKCTRL_CT16B0; /* power on */
	regs[REG_TMR16B0TCR] = 2; /* reset counters, disable */
	regs[REG_TMR16B0PR] = (CPU_FREQ / 1000 / 1000) - 1; /* 1us */
	regs[REG_TMR16B0MR0] = 49; /* match when counter: 50us */
	regs[REG_TMR16B0MCR] = 3; /* irq and reset counter */
	regs[REG_TMR16B0TCR] = 1; /* enable */

	/* no irq_request() nor enable: it's all centarlly managed */
	t->irq = LPC_IRQ_CT16B0;
	t->flags |= TASK_FLAG_IRQ;
	return 0;
}

static void shell_led_task(void *arg, struct task *t)
{
	static unsigned  long last_change;
	static int pwm_phase;
	struct shell_led_cfg *d = arg;

	regs[REG_TMR16B0IR] = 1; /* Ack irq: MR0 */

	if (!last_change) /* first invocation */
		last_change = jiffies - d->jiffies_each;

	/* If it's time to change led, do it */
	if (time_after_eq(jiffies, last_change + d->jiffies_each)) {
		last_change = jiffies;
		pwm_phase = 0;
		gpio_set(shell_led_gpio[d->current_led], 0);
		d->current_led++;
		if (d->current_led == ARRAY_SIZE(shell_led_gpio))
			d->current_led = 0;
		gpio_set(shell_led_gpio[d->current_led], 1);
	}
	/* Only for led zero, run the pwm stuff  */
	if (d->current_led == 0) {
		if (pwm_phase >= d->pwm_total)
			pwm_phase = 0;
		gpio_set(shell_led_gpio[d->current_led],
			 pwm_phase < d->pwm_high);
		pwm_phase++;
	}
}

/* And it is a task too */
struct task __task t_leds = {
	.name = "leds",
	.init = shell_led_init,
	.job = shell_led_task,
	.arg = &shell_led_cfg,
};

/* The shell is now a task, polling the uart every ms */
void shell_task(void *arg, struct task *self)
{
	static char str[80];
	struct command *commands = arg;

	if (!polls(str, sizeof(str)))
		return;

	command_parse(str, commands);
	puts(command_reply);
	str[0] = '\0';
}

struct task __task t_shell = {
	.name = "shell",
	.job = shell_task,
	.arg = shell_commands,
	.period = HZ / 1000 + 1, /* "+ 1" to prevent a 0 period */
};

void main(void)
{
	printf("%s: built on %s-%s\n", __FILE__, __DATE__, __TIME__);

	/* Just run */
	scheduler(SCHED_VERBOSE_INIT);
}
