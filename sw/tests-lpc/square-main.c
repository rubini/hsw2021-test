#include <io.h>
#include <irq.h>
/*
 * Configure an interrupt every 10ms, using a 16-bit timer.
 * Then output a square wave to PIO0_16 (the "pps" pin in the TDC board)
 */
#define PIN GPIO_NR(0, 16)

void __irq_entry(void)
{
	/* do nothing, just ack both the peripheral and the NVIC */
	regs[REG_TMR16B0IR] = 1; /* MR0 */
	regs[REG_NVIC_ICPR0] = lpc_irq_bit(LPC_IRQ_CT16B0);
}

void main(void)
{

	/* Before running the square wave, configure a periodic timer */
	regs[REG_AHBCLKCTRL] |= REG_AHBCLKCTRL_CT16B0; /* power on */
	regs[REG_TMR16B0TCR] = 2; /* reset counters, disable */
	regs[REG_TMR16B0PR] = (CPU_FREQ / 1000) - 1; /* prescaler: 1kHz */
	regs[REG_TMR16B0MR0] = 10; /* match when counter = 10 */
	regs[REG_TMR16B0MCR] = 3; /* irq and reset counter */
	regs[REG_TMR16B0TCR] = 1; /* enable */

	irq_enable();

	/* And enable the associated interrupt */
	regs[REG_NVIC_ISER0] = lpc_irq_bit(LPC_IRQ_CT16B0);

	gpio_dir_af(PIN, GPIO_DIR_OUT, 0, GPIO_AF_GPIO);
	while (1) {
		writel(1, __GPIO_WORD(PIN));
		writel(0, __GPIO_WORD(PIN));
	}
}
