#include <time.h>
#include <neopixel.h>

void main(void)
{
	unsigned long j = jiffies;

	gpio_dir_af(GPIO_NR(1, 31), GPIO_DIR_OUT, 0, GPIO_AF_GPIO);

	while (1) {
		neopix_rgb(0x20, 0x00, 0x00);
		j += HZ;
		while (time_before(jiffies, j))
		       ;

		neopix_rgb(0x00, 0x20, 0x00);
		j += HZ;
		while (time_before(jiffies, j))
		       ;

		neopix_rgb(0x00, 0x00, 0x20);
		j += HZ;
		while (time_before(jiffies, j))
		       ;
	}
}
