#include <io.h>
#include <spi.h>
#include <time.h>
#include <string.h>
#include <command.h>
#include <errno.h>

static const struct spi_cfg spi_cfg = {
	.gpio_cs = GPIO_NR(1, 25),
	.freq = 100 * 1000,
	.timeout = HZ / 10,
	.devn = 1,
};

static struct spi_dev spi_dev = {
	.cfg = &spi_cfg,
}, *sd;

static int command_spi(char **reply, int argc, char **argv)
{
	static uint8_t bi[20];
	static uint8_t bo[20];
	struct spi_ibuf ibuf;
	struct spi_obuf obuf;
	int i, val;
	char c, *s = *reply;

	if (argc < 1)
		return -EINVAL;
	argv++;
	argc--;
	for (i = 0; i < argc; i++) {
		if (sscanf(argv[i], "%x%c", &val, &c) != 1)
			return -EINVAL;
		bo[i] = val;
	}
	ibuf.len = obuf.len = argc;
	ibuf.buf = bi; obuf.buf = bo;
	i = spi_xfer(sd, 0, &ibuf, &obuf);
	if (i < 0)
		return i;
	for (i = 0; i < argc; i++)
		s += sprintf(s, "%s%02x", i ? " " : "", bi[i]);
	return 0;
}

static struct command commands[] = {
	COMMAND_R,
	COMMAND_W,
	COMMAND_GPIO,
	COMMAND_HELP,
	{"spi",    command_spi,      2, 20},
	{}
};

void main(void)
{
	static char str[80];
	printf("%s: built on %s-%s\n", __FILE__, __DATE__, __TIME__);

	sd = spi_create(&spi_dev);
	if (!sd)
		printf("Can't create SPI device\n");

	while (1) {

		if (polls(str, sizeof(str))) {
			command_parse(str, commands);
			puts(command_reply);
			str[0] = '\0';
		}
	}
}
