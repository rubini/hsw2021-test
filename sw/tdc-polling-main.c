/*
 * This our first "tdc" thing. We poll the input bitmask and print
 */
#include <io.h>
#include <time.h>
#include <timestamp-lpc.h>

struct tdc_event {
	uint32_t oldmask, newmask;
	struct timestamp ts;
};

struct tdc_event  events[CONFIG_TDC_BUFFER_SIZE];

static uint32_t tdc_mask = 0x00f1f000; /* the mask of the valid bits */

static inline int tdc_shrink(uint32_t m)
{ return m >> 12; }  /* base on the above */

static void tdc_print(struct tdc_event *e)
{
	unsigned long ticks, nanos, d1, d2;

	ticks = timestamp_to_64(&e->ts);
	nanos = timestamp_to_ns(&e->ts);
	printf("%03x -> %03x   t %10lu   ns %10lu",
	       tdc_shrink(e->oldmask), tdc_shrink(e->newmask),
	       ticks, nanos);

	       /* Hack: refer to global events array */
	       if (e != events) {
		       d1 = ticks - timestamp_to_64(&e[-1].ts);
		       d2 = nanos - timestamp_to_ns(&e[-1].ts);
		       printf(" (delta %5li %9li)", d1, d2);
	       }
	       printf("\n");
}

void main(void)
{
	int i;
	struct tdc_event *e;
	uint32_t oldmask, newmask;

	printf("TDC polling (commit %s)\n", __GITC__);

	/* Activate pull-down on all bits, to prevent noise */
	for (i = 0; i < 31; i++) {
		if (tdc_mask & (1 << i)) {
			gpio_dir_af(GPIO_NR(0, i), GPIO_DIR_IN, 0,
				    GPIO_AF_GPIO | GPIO_AF_PULLDOWN);
		}
	}

	/* setup mask reading (only some bits are returned */
	regs[REG_GPIO_MASK0] = ~tdc_mask;

	while(1) {
		oldmask = regs[REG_GPIO_MPIN0];
		for (i = 0, e = events; i < CONFIG_TDC_BUFFER_SIZE; i++, e++) {
			while ((newmask = regs[REG_GPIO_MPIN0]) == oldmask)
				;
			timestamp_get(&e->ts);
			e->oldmask = oldmask;
			e->newmask = newmask;
			oldmask = newmask;
		}

		for (i = 0, e = events; i < CONFIG_TDC_BUFFER_SIZE; i++, e++)
			tdc_print(e);
		printf("\n");
	}
}
