.extern setup
.extern main
.extern _fstack
.extern panic

/*
 * This file includes the initial assembly stuff. It sets the stack
 * pointer and zeroes the BSS. For rom-boot it also copies data to RAM.
 *
 * Different ELF sections are used to differentiate the behaviour
 */

/* The vectors are used when booting from flash */
.section .vectors, "ax"
	.type vectors, %object
	.global vectors
vectors:
	/* The first vector in Cortex is the stack pointer */
	.word _fstack
	/* The second one is reset */
	.word _entry_rom + 1
	.rep 9 /* 2..10 */
	.word __irq_entry + 1
	.endr
	.word svc_entry + 1
	.rep 36 /* 12..47 */
	.word __irq_entry + 1
	.endr
.size vectors, . - vectors

/* RAM boot: load stack pointer, and go to clearing bss */
.section .text.ramboot, "ax"
	.type _entry_ram, %function
	.global _entry_ram
_entry_ram:
	ldr	r0, =_fstack
	mov	sp, r0
	b	_bss_loop
.ltorg	
	/*
	 * By aligning to the next multiple of 32 bytes, we allow
	 * tools/fix-checksum to modify this binary without damaging code
	 */
.align 5, 0xaa
.size _entry_ram, . - _entry_ram

/* ROM boot: copy data to ram. SP is already set be vectors[0] */
.section .text.romboot, "ax"
	.type _entry_rom, %function
	.global _entry_rom
_entry_rom:
	ldr	r0, =_sdata
	ldr	r1, =_edata
	ldr	r2, =_erom
	/* copy from r2 to r0, until r1. We know it's aligned at 16b */
10:
	cmp	r0, r1
	bge	2f
	ldmia	r2!, {r4, r5, r6, r7}
	stmia	r0!, {r4, r5, r6, r7}
	b	10b
2:
	b	_bss_loop
.ltorg
.size _entry_rom, . - _entry_rom


/* This is the weak default __irq_entry, that can be overridden by C code */
.section .text.__irq_entry, "ax"
	.type __irq_entry, %function
	.global __irq_entry
	.weak __irq_entry
__irq_entry: /* default (weak) code: calls irq_entry(irq, tstamp_fraction) */
	ldr	r0, =#0x40018010
	ldr	r1, [r0]
	mov	r2, #0x3f
	mrs	r0, psr
	and	r0, r2
	b	irq_entry
.size __irq_entry, . - __irq_entry


/* And  the weak default irq_entry(irq, stamp), again, can be overridden */
.section .text.irq_entry, "ax"
	.type irq_unexpected, %function
	.global irq_unexpected
	.global irq_entry
	.weak irq_entry
irq_entry:
irq_unexpected: /* default irq_enty code: calls panic(129, "... %i\n", irq) */
	mov	r2, r0
	mov	r0, #129
	ldr	r1, =string_unexpected_irq
	bl	panic
.size irq_unexpected, . - irq_unexpected


.section .text.svc_entry, "ax"
	.type svc_entry, %function
	.global svc_entry
	.weak svc_entry
svc_entry: /* panic, as above */
	mov	r0, #130
	mov	r2, #11 /* svc == irq 11 */
	ldr	r1, =string_unexpected_irq
	bl	panic
.size svc_entry, . - svc_entry


/* The following is common to both ROM and RAM situations */
.section .text.boot, "ax"
_bss_loop:
	ldr	r1, =__bss_start
	ldr	r2, =__bss_end
	mov	r0, #0
0:
	cmp	r1, r2
	bge	1f
	str	r0, [r1]
	add	r1, #4
	b	0b

1:
	/* Pre-fill the stack so we can detect usage */	
	ldr	r1, =_stack
	ldr	r2, =_fstack
	ldr	r0, =0xfac0ffee
0:
	cmp	r1, r2
	bge	1f
	str	r0, [r1]
	add	r1, #4
	b	0b

1:
	bl setup
	bl main
	mov	r0, #128
	ldr	r1, =string_main_returned
	bl	panic

.section .rodata
string_main_returned:
	.ascii "Main returned\n"
.section .rodata.unxexp_irq
string_unexpected_irq:
	.ascii "Unexpected irq %i\n"
