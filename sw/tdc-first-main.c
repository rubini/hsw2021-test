/*
 * This our first "tdc" thing. We poll the input bitmask and print
 */
#include <io.h>
#include <time.h>

/* Hardware dependent: the description of our input lines in port  0 */
static uint32_t tdc_mask = 0x00f1f000;


void main(void)
{
	int i;
	uint32_t oldmask, newmask;
	unsigned long oldj, j;
	const long usj = 1000 * 1000 / HZ;

	printf("TDC polling (commit %s)\n", __GITC__);
	printf("HZ is now %i (%li usecs per jiffy)\n", HZ, usj);
	if (usj * HZ != 1000 * 1000)
		printf("** Warning: time calculation is wrong\n");

	/* configure our input bits */
	for (i = 0; i < 32; i++) {
		if (tdc_mask & (1 << i)) {
			gpio_dir_af(GPIO_NR(0, i), GPIO_DIR_IN, 0,
				    GPIO_AF_GPIO | GPIO_AF_PULLDOWN);
		}
	}

	/* setup mask reading (only some bits are returned) */
	regs[REG_GPIO_MASK0] = ~tdc_mask;

	oldmask = regs[REG_GPIO_MPIN0];
	oldj = jiffies;
	while(1) {
		/* wait for a change */
		do {
			newmask = regs[REG_GPIO_MPIN0];
			j = jiffies;
		} while (oldmask == newmask);

		/* report the change */
		printf("%9li us: %08x -> %08x\n",
		       (j - oldj) * usj, (int)oldmask, (int)newmask);
		oldj = j;
		oldmask = newmask;
	}
}
