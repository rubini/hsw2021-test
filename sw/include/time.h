#ifndef __TIME_H__
#define __TIME_H__
#include <stdint.h>

#define HZ CONFIG_HZ

#ifndef jiffies
	extern volatile unsigned long jiffies;
#endif

extern void udelay(uint32_t usec);

/* The timestamp is architecture-specific, but based on a consistent struct */
struct timestamp {
	uint32_t counts;
	uint32_t fraction;
};

/* And the functions, too, must obey the same prototype */
extern void timestamp_get(struct timestamp *t);
extern uint64_t timestamp_to_64(struct timestamp *t);
extern uint64_t timestamp_to_ns(struct timestamp *t);
extern uint64_t timestamp_get_64(void);
extern uint64_t timestamp_get_ns(void);


/* The following ones come from the kernel, but simplified */
#define time_after(a,b)         \
        ((long)(b) - (long)(a) < 0)
#define time_before(a,b)        time_after(b,a)
#define time_after_eq(a,b)      \
         ((long)(a) - (long)(b) >= 0)
#define time_before_eq(a,b)     time_after_eq(b,a)

#define time_in_range(a,b,c) \
        (time_after_eq(a,b) && \
         time_before_eq(a,c))

#endif /* __TIME_H__ */
