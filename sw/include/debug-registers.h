#ifndef __DEBUG_REGISTERS_H__
#define __DEBUG_REGISTERS_H__

struct cpu_regs {
	uint32_t r[16];
	uint32_t msp, psp, psr, primask, control;
};

extern struct cpu_regs cpu_regs;

extern void registers_print(struct cpu_regs *r);

static inline void registers_save(void)
{
	/* This saves the low registers. High registers can't use stmia */
	asm volatile(
		"push {r0}\n\t"
		"ldr r0, =cpu_regs\n\t"
		"stmia r0!, {r0-r7}\n\t"
		"ldr r1, =cpu_regs\n\t"
		"pop {r0}\n\t"
		"str r0, [r1]\n\t"
		"ldr r1, [r1, #4]\n\t" : : : "cc");

	/* High registers */
	asm volatile(
		"push {r0, r1}\n\t"
		"ldr r1, =cpu_regs\n\t"
		"add r1, #32\n\t"
		"mov r0, r8\n\tstr r0, [r1]\n\tadd r1, #4\n\t"
		"mov r0, r9\n\tstr r0, [r1]\n\tadd r1, #4\n\t"
		"mov r0, r10\n\tstr r0, [r1]\n\tadd r1, #4\n\t"
		"mov r0, r11\n\tstr r0, [r1]\n\tadd r1, #4\n\t"
		"mov r0, r12\n\tstr r0, [r1]\n\tadd r1, #4\n\t"
		"mov r0, sp\n\tadd r0,#8\n\tstr r0, [r1]\n\tadd r1, #4\n\t"
		"mov r0, lr\n\tstr r0, [r1]\n\tadd r1, #4\n\t"
		"mov r0, pc\n\tstr r0, [r1]\n\t"
		"pop {r0, r1}\n\t"
		: : : "cc");

	/* Special registers */
	asm volatile(
		"ldr r1, =cpu_regs\n\t"
		"add r1, #64\n\t"
		"mrs r0, msp\n\tstr r0, [r1]\n\tadd r1, #4\n\t"
		"mrs r0, psp\n\tstr r0, [r1]\n\tadd r1, #4\n\t"
		"mrs r0, psr\n\tstr r0, [r1]\n\tadd r1, #4\n\t"
		"mrs r0, primask\n\tstr r0, [r1]\n\tadd r1, #4\n\t"
		"mrs r0, control\n\tstr r0, [r1]\n\tadd r1, #4\n\t"
		"ldr r1, =cpu_regs\n\t"
		"ldr r0, [r1]\n\t"
		"ldr r1, [r1, #4]"
		: : : "cc");

}

#endif /* __DEBUG_REGISTERS_H__ */
