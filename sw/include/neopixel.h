#ifndef __NEOPIXEL_H__
#define __NEOPIXEL_H__

extern void neopix_rgb(uint8_t r, uint8_t g, uint8_t b);

#endif /* __NEOPIXEL_H__ */
