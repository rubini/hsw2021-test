#ifndef __IO_H__
#define __IO_H__
#include <stdint.h>
#include <stdarg.h>

/* basic macros, that should live in stddef.h */
#define ARRAY_SIZE(x) (sizeof(x)/sizeof(x[0]))

#define NULL 0

#if 0
    /* basic array for register access. Nice but inefficient */
    extern volatile uint32_t regs[];
#else
    /* much less beautiful, but we save reading 0 into a register */
    #define regs ((volatile uint32_t *)0)
#endif

/* readl/writel (linux style) which sometimes are useful shorthands */
static inline uint32_t readl(unsigned long reg)
{
	return regs[reg / 4];
}
static inline void writel(uint32_t val, unsigned long reg)
{
	regs[reg / 4] = val;
}

extern int puts(const char *s);
extern int putc(int c);
/*
 * stdio-lookalike follows
 */
#include "../pp_printf/pp-printf.h"

#define NULL 0

extern int puts(const char *s);
extern int putc(int c);

/* serial input: getc/gets are blocking (beware!) */
extern int getc(void);
extern char *gets(char *s); /* includes trailing newline */

/* non-blocking serial input, returns NULL or -EAGAIN if not ready */
extern int pollc(void);
extern char *polls(char *s, int bsize); /* pass empty string! */
extern char *polls_t(char *s, char *terminators, int bsize);

/* We turn printf to pp_printf and so on. But offer "std" prototypes here */
extern int printf(const char *fmt, ...)
        __attribute__((format(printf,1,2)));

extern int sprintf(char *s, const char *fmt, ...)
        __attribute__((format(printf,2,3)));

extern int vprintf(const char *fmt, va_list args);

extern int vsprintf(char *buf, const char *, va_list)
        __attribute__ ((format (printf, 2, 0)));

/* Standard prototypes for sscanf, which is really pp_sscanf in the code */
extern int vsscanf(const char *buf, const char *fmt, va_list args);

extern int sscanf(const char *buf, const char *fmt, ...)
        __attribute__((format(scanf, 2, 3)));

#endif /* __IO_H__  */
