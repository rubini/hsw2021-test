#ifndef __BOARD_H__
#define __BOARD_H__
/*
 * Here we have all board-specific definitions. This is an unavoidable
 * step, at some point (not everything can be done in Kconfig).
 *
 * If you support several boards in the same project, you can use
 * different board.h files, selected by Kconfig variables.  But please
 * be careful about unbuilt code: you'll need a "makeall" script to
 * build test all boards every time you make a non-trivial change to
 * your code base.
 */

#define GPIO_NP GPIO_NR(0, 9)

/* the neopixel code depends on the following two macros */
#define NEOPIX_LOWER()   __gpio_set(GPIO_NP, 1)
#define NEOPIX_RAISE()   __gpio_set(GPIO_NP, 0)

#endif /* __BOARD_H__ */
