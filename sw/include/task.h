#ifndef __TASK_H__
#define __TASK_H__
#include <time.h>

struct task {
	const char *name;
	void *arg;
	int (*init)(void *arg, struct task *t);
	void (*job)(void *arg, struct task *t);
	unsigned long release, period;
	struct timestamp start_time;
	unsigned long wcet;
	unsigned short flags;
	unsigned char irq, prio;
};

extern struct task *current; /* the scheduler tracks this */

/* If the user leaves "prio" to zero, the following ones apply */
#define TASK_PRIO_USER_DEFAULT		100
#define TASK_PRIO_IRQ_DEFAULT		150

/* An interrupt thread must set the flag too (irq 0 is valid) */
#define TASK_FLAG_IRQ		0x0001

/* Internal flags, managed by the scheduler */
#define __TASK_FLAG_ACTIVE	0x0100 /* only for irq ones */


#define __task __attribute__((section(".task"),__used__))

/* If you use tasks, at some point you need to call the scheduler */
extern void __attribute__((noreturn))  scheduler(int verbose);
#define SCHED_VERBOSE_INIT 0x40
#define SCHED_VERBOSE_CALL 0x80

#endif /* __TASK_H__ */
