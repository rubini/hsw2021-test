#ifndef __TIMESTAMP_H__
#define __TIMESTAMP_H__
#include <io.h>
#include <time.h> /* we use a shared structure across architectures */

static inline void __timestamp_get(struct timestamp *t)
{
	uint32_t unused;

	/* We know for sure that jiffies is timer 32B1 */
	asm("ldr   %2,    [%3, #8]\n\t"
	    "ldr   %1,    [%3, #16]\n\t"
	    "ldr   %0,    [%3, #8]\n\t"
	    "cmp   %0,    %2\n\t"
	    "beq   1f\n\t"
	    "movs  %1,    #0\n\t"
	    "1:"
	    : "=l" (t->counts), "=l" (t->fraction) , "=l" (unused)
	    : "l" (0x40018000)
	    : "cc");
}

#endif /* __TIMESTAMP_H__ */
