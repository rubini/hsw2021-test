#include <neopixel.h>
#include <time.h>
#include <io.h>
#include <stringify.h>

#define CPU_FREQ_S __stringify(CPU_FREQ)

#include <board.h>

/* provide a build-time check for undefined macros, if used */
extern int please_provide_neopixel_low_level_io;
#ifndef NEOPIX_RAISE
#define NEOPIX_RAISE() please_provide_neopixel_low_level_io++
#endif
#ifndef NEOPIX_LOWER
#define NEOPIX_LOWER() please_provide_neopixel_low_level_io++
#endif

/*
 * Calculations here are based on ideas set forth in
 * http://wp.josh.com/2014/05/13/
 *     ws2812-neopixels-are-not-so-finicky-once-you-get-to-know-them/
 *
 *   Symbol     Parameter                     Min   Typical   Max
 *    T0H     0 code ,high voltage time       200     350      500
 *    T1H     1 code ,high voltage time       550     700     5500
 *    TLD     data, low voltage time          450     600     5000
 *    TLL     latch, low voltage time        6000
 */

static void neopix_reset(void)
{
	NEOPIX_LOWER();
	udelay(15);
}

static __attribute__((__noinline__)) void bit_1(void)
{
	/* 550ns min: use clock frequency divided by 2M + 1 */
	NEOPIX_RAISE();
	asm(".set __neopixel_count_1, " CPU_FREQ_S " / 1000 / 1000 / 2 + 1\n");
	asm(".rep __neopixel_count_1\n\tnop\n\t.endr\n");
	NEOPIX_LOWER();
}

static __attribute__((__noinline__)) void bit_0(void)
{
	/* 200ns - 500ns: 1 nop every clock means FREQ / 5M, and overhead */
	NEOPIX_RAISE();
	asm(".set __neopixel_count_0, " CPU_FREQ_S " / 1000 / 1000 / 5 - 1\n");
	asm(".rep __neopixel_count_0\n\tnop\n\t.endr\n");
	NEOPIX_LOWER();
}

static void neopix_byte(uint8_t v)
{
	uint8_t i;

	for (i = 0x80; i; i >>= 1) {
		if (v & i)
			bit_1();
		else
			bit_0();
		if (CPU_FREQ > 16000 * 1000) {
			/* wait a little while in low state: use t0 count */
			asm(".set __neopixel_count_low, " CPU_FREQ_S
			    " / 1000 / 1000 / 5 - 1\n");
			asm(".rep __neopixel_count_low\n\tnop\n\t.endr\n");
		}
	}
}

void neopix_rgb(uint8_t r, uint8_t g, uint8_t b)
{
	/* We have three leds: do it three times */
	int i;

	gpio_dir_af(GPIO_NP, GPIO_DIR_OUT, 1, GPIO_AF_GPIO);

	for (i = 0; i < 3; i++) {
		neopix_byte(g);
		neopix_byte(r);
		neopix_byte(b);
	}
	neopix_reset();
}
