#include <io.h>
#include <time.h>
#include <neopixel.h>
#include "revision.h"

#define LED_1		GPIO_NR(0,  7)
#define LED_2		GPIO_NR(1, 28)
#define LED_3		GPIO_NR(1, 31)

#define NEOPIX		GPIO_NR(0, 9)

#define BUZZ		GPIO_NR(0, 8)
#define RELAY		GPIO_NR(1, 14)

uint8_t np_values[9];

static void self_test(void)
{
	int i;

	gpio_dir_af(LED_1, GPIO_DIR_OUT, 0, GPIO_AF_GPIO);
	gpio_dir_af(LED_2, GPIO_DIR_OUT, 0, GPIO_AF_GPIO);
	gpio_dir_af(LED_3, GPIO_DIR_OUT, 0, GPIO_AF_GPIO);
	gpio_dir_af(NEOPIX, GPIO_DIR_OUT, 1, GPIO_AF_GPIO);
	gpio_dir_af(BUZZ, GPIO_DIR_OUT, 0, GPIO_AF_GPIO);
	gpio_dir_af(RELAY, GPIO_DIR_OUT, 0, GPIO_AF_GPIO);

	/* Diag leds */
	gpio_set(LED_1, 1);
	udelay(500 * 1000);
	gpio_set(LED_1, 0);
	gpio_set(LED_2, 1);
	udelay(500 * 1000);
	gpio_set(LED_2, 0);
	gpio_set(LED_3, 1);
	udelay(500 * 1000);
	gpio_set(LED_3, 0);

	/* Neopixels */
	for (i = 0; i < ARRAY_SIZE(np_values); i++) {
		np_values[i] = 0x08;
		neopix_array(np_values, ARRAY_SIZE(np_values), 1);
		udelay(200*1000);
	}
	for (i = 0; i < ARRAY_SIZE(np_values); i++) {
		np_values[i] = 0x0;
		neopix_array(np_values, ARRAY_SIZE(np_values), 1);
		udelay(200*1000);
	}

	/* Buzzer: 250Hz, 0.5s */
	for (i = 0; i < 250; i++) {
		gpio_set(BUZZ, 1);
		udelay(2000);
		gpio_set(BUZZ, 0);
		udelay(2000);
	}
	/* Relay: tic-tac tic tac */
	udelay(300 * 1000);
	gpio_set(BUZZ, 1);
	udelay(300 * 1000);
	gpio_set(BUZZ, 0);
	udelay(300 * 1000);
	gpio_set(BUZZ, 1);
	udelay(300 * 1000);
	gpio_set(BUZZ, 0);
	udelay(300 * 1000);
}

void main(void)
{
	unsigned long j;

	printf("Hello: I am %s (commit %s)\n", __FILE__,
	       __gitc__);

	while (1) {
		self_test();
		j = jiffies + HZ;
		while (time_before(jiffies, j))
			;
	}
}
