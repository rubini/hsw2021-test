EESchema Schematic File Version 2
LIBS:rubi-conn
LIBS:rubi-cpu
LIBS:rubi-discrete
LIBS:rubi-ic
LIBS:rubi-sensors
LIBS:rubi-modules
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:tdc-cache
EELAYER 27 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title ""
Date "7 mar 2021"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L WS2812B U201
U 1 1 604487D3
P 2925 1275
F 0 "U201" H 2925 1100 50  0000 C CNN
F 1 "WS2812B" H 2925 1425 50  0000 C CNN
F 2 "~" H 2925 1275 60  0000 C CNN
F 3 "~" H 2925 1275 60  0000 C CNN
	1    2925 1275
	-1   0    0    -1  
$EndComp
$Comp
L WS2812B U202
U 1 1 60448813
P 3625 1275
F 0 "U202" H 3625 1100 50  0000 C CNN
F 1 "WS2812B" H 3625 1425 50  0000 C CNN
F 2 "~" H 3625 1275 60  0000 C CNN
F 3 "~" H 3625 1275 60  0000 C CNN
	1    3625 1275
	-1   0    0    -1  
$EndComp
$Comp
L WS2812B U203
U 1 1 6044881E
P 4325 1275
F 0 "U203" H 4325 1100 50  0000 C CNN
F 1 "WS2812B" H 4325 1425 50  0000 C CNN
F 2 "~" H 4325 1275 60  0000 C CNN
F 3 "~" H 4325 1275 60  0000 C CNN
	1    4325 1275
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR056
U 1 1 6044A081
P 2575 1600
F 0 "#PWR056" H 2575 1600 30  0001 C CNN
F 1 "GND" H 2575 1530 30  0001 C CNN
F 2 "" H 2575 1600 60  0000 C CNN
F 3 "" H 2575 1600 60  0000 C CNN
	1    2575 1600
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR057
U 1 1 6044A09A
P 4825 925
F 0 "#PWR057" H 4825 1015 20  0001 C CNN
F 1 "+5V" H 4825 1015 30  0000 C CNN
F 2 "" H 4825 925 60  0000 C CNN
F 3 "" H 4825 925 60  0000 C CNN
	1    4825 925 
	1    0    0    -1  
$EndComp
NoConn ~ 4575 1325
$Comp
L NMOS Q201
U 1 1 6044A0ED
P 2175 1350
F 0 "Q201" H 2275 1350 40  0000 C CNN
F 1 "2N27002" H 1975 1500 40  0000 C CNN
F 2 "~" H 2175 1350 60  0000 C CNN
F 3 "~" H 2175 1350 60  0000 C CNN
	1    2175 1350
	1    0    0    -1  
$EndComp
$Comp
L R-SMALL R201
U 1 1 6044A12C
P 2500 1000
F 0 "R201" H 2500 1100 40  0000 C CNN
F 1 "1k5" H 2500 1000 40  0000 C CNN
F 2 "~" H 2500 1000 60  0000 C CNN
F 3 "~" H 2500 1000 60  0000 C CNN
	1    2500 1000
	1    0    0    -1  
$EndComp
Text HLabel 1650 1350 0    40   Input ~ 0
~NP
$Comp
L STRIP-6 J201
U 1 1 6044A68A
P 3050 4650
F 0 "J201" H 2650 4650 40  0000 C CNN
F 1 "analog-in" H 3050 4750 40  0000 C CNN
F 2 "~" H 2850 4650 60  0000 C CNN
F 3 "~" H 2850 4650 60  0000 C CNN
	1    3050 4650
	0    1    1    0   
$EndComp
$Comp
L R-SMALL R202
U 1 1 6044A6BB
P 1575 2825
F 0 "R202" H 1575 2925 40  0000 C CNN
F 1 "100k" H 1575 2825 40  0000 C CNN
F 2 "~" H 1575 2825 60  0000 C CNN
F 3 "~" H 1575 2825 60  0000 C CNN
	1    1575 2825
	-1   0    0    -1  
$EndComp
$Comp
L R-SMALL R204
U 1 1 6044A6C1
P 1575 3075
F 0 "R204" H 1575 3175 40  0000 C CNN
F 1 "47k" H 1575 3075 40  0000 C CNN
F 2 "~" H 1575 3075 60  0000 C CNN
F 3 "~" H 1575 3075 60  0000 C CNN
	1    1575 3075
	-1   0    0    -1  
$EndComp
$Comp
L CSMALL C201
U 1 1 6044A6D3
P 1575 3300
F 0 "C201" V 1650 3175 40  0000 L CNN
F 1 "100pF" V 1450 3225 40  0000 L CNN
F 2 "~" H 1575 3300 60  0000 C CNN
F 3 "~" H 1575 3300 60  0000 C CNN
	1    1575 3300
	0    1    -1   0   
$EndComp
$Comp
L DIODESCH D201
U 1 1 6044AD11
P 1625 2500
F 0 "D201" H 1625 2600 40  0000 C CNN
F 1 "RB751V40" H 1625 2400 40  0000 C CNN
F 2 "~" H 1625 2500 60  0000 C CNN
F 3 "~" H 1625 2500 60  0000 C CNN
	1    1625 2500
	1    0    0    1   
$EndComp
$Comp
L GND #PWR058
U 1 1 6044ADCF
P 2025 3150
F 0 "#PWR058" H 2025 3150 30  0001 C CNN
F 1 "GND" H 2025 3080 30  0001 C CNN
F 2 "" H 2025 3150 60  0000 C CNN
F 3 "" H 2025 3150 60  0000 C CNN
	1    2025 3150
	-1   0    0    -1  
$EndComp
$Comp
L +3.3V #PWR059
U 1 1 6044AE19
P 1950 2400
F 0 "#PWR059" H 1950 2360 30  0001 C CNN
F 1 "+3.3V" H 1950 2510 30  0000 C CNN
F 2 "" H 1950 2400 60  0000 C CNN
F 3 "" H 1950 2400 60  0000 C CNN
	1    1950 2400
	-1   0    0    -1  
$EndComp
$Comp
L R-SMALL R205
U 1 1 6044AE55
P 1575 4125
F 0 "R205" H 1575 4225 40  0000 C CNN
F 1 "100k" H 1575 4125 40  0000 C CNN
F 2 "~" H 1575 4125 60  0000 C CNN
F 3 "~" H 1575 4125 60  0000 C CNN
	1    1575 4125
	-1   0    0    -1  
$EndComp
$Comp
L R-SMALL R207
U 1 1 6044AE5B
P 1575 4375
F 0 "R207" H 1575 4475 40  0000 C CNN
F 1 "47k" H 1575 4375 40  0000 C CNN
F 2 "~" H 1575 4375 60  0000 C CNN
F 3 "~" H 1575 4375 60  0000 C CNN
	1    1575 4375
	1    0    0    -1  
$EndComp
$Comp
L CSMALL C202
U 1 1 6044AE61
P 1575 4600
F 0 "C202" V 1650 4475 40  0000 L CNN
F 1 "100pF" V 1450 4525 40  0000 L CNN
F 2 "~" H 1575 4600 60  0000 C CNN
F 3 "~" H 1575 4600 60  0000 C CNN
	1    1575 4600
	0    1    -1   0   
$EndComp
$Comp
L DIODESCH D202
U 1 1 6044AE67
P 1625 3800
F 0 "D202" H 1625 3900 40  0000 C CNN
F 1 "RB751V40" H 1625 3700 40  0000 C CNN
F 2 "~" H 1625 3800 60  0000 C CNN
F 3 "~" H 1625 3800 60  0000 C CNN
	1    1625 3800
	1    0    0    1   
$EndComp
$Comp
L GND #PWR060
U 1 1 6044AE78
P 2025 4450
F 0 "#PWR060" H 2025 4450 30  0001 C CNN
F 1 "GND" H 2025 4380 30  0001 C CNN
F 2 "" H 2025 4450 60  0000 C CNN
F 3 "" H 2025 4450 60  0000 C CNN
	1    2025 4450
	-1   0    0    -1  
$EndComp
$Comp
L +3.3V #PWR061
U 1 1 6044AE80
P 1950 3700
F 0 "#PWR061" H 1950 3660 30  0001 C CNN
F 1 "+3.3V" H 1950 3810 30  0000 C CNN
F 2 "" H 1950 3700 60  0000 C CNN
F 3 "" H 1950 3700 60  0000 C CNN
	1    1950 3700
	-1   0    0    -1  
$EndComp
$Comp
L R-SMALL R208
U 1 1 6044AE8E
P 1575 5425
F 0 "R208" H 1575 5525 40  0000 C CNN
F 1 "100k" H 1575 5425 40  0000 C CNN
F 2 "~" H 1575 5425 60  0000 C CNN
F 3 "~" H 1575 5425 60  0000 C CNN
	1    1575 5425
	-1   0    0    -1  
$EndComp
$Comp
L R-SMALL R210
U 1 1 6044AE94
P 1575 5675
F 0 "R210" H 1575 5775 40  0000 C CNN
F 1 "47k" H 1575 5675 40  0000 C CNN
F 2 "~" H 1575 5675 60  0000 C CNN
F 3 "~" H 1575 5675 60  0000 C CNN
	1    1575 5675
	-1   0    0    -1  
$EndComp
$Comp
L CSMALL C203
U 1 1 6044AE9A
P 1575 5900
F 0 "C203" V 1650 5775 40  0000 L CNN
F 1 "100pF" V 1450 5825 40  0000 L CNN
F 2 "~" H 1575 5900 60  0000 C CNN
F 3 "~" H 1575 5900 60  0000 C CNN
	1    1575 5900
	0    1    -1   0   
$EndComp
$Comp
L DIODESCH D203
U 1 1 6044AEA0
P 1625 5100
F 0 "D203" H 1625 5200 40  0000 C CNN
F 1 "RB751V40" H 1625 5000 40  0000 C CNN
F 2 "~" H 1625 5100 60  0000 C CNN
F 3 "~" H 1625 5100 60  0000 C CNN
	1    1625 5100
	1    0    0    1   
$EndComp
$Comp
L GND #PWR062
U 1 1 6044AEB1
P 2025 5750
F 0 "#PWR062" H 2025 5750 30  0001 C CNN
F 1 "GND" H 2025 5680 30  0001 C CNN
F 2 "" H 2025 5750 60  0000 C CNN
F 3 "" H 2025 5750 60  0000 C CNN
	1    2025 5750
	-1   0    0    -1  
$EndComp
$Comp
L +3.3V #PWR063
U 1 1 6044AEB9
P 1950 5000
F 0 "#PWR063" H 1950 4960 30  0001 C CNN
F 1 "+3.3V" H 1950 5110 30  0000 C CNN
F 2 "" H 1950 5000 60  0000 C CNN
F 3 "" H 1950 5000 60  0000 C CNN
	1    1950 5000
	-1   0    0    -1  
$EndComp
$Comp
L R-SMALL R211
U 1 1 6044AEC7
P 1575 6725
F 0 "R211" H 1575 6825 40  0000 C CNN
F 1 "100k" H 1575 6725 40  0000 C CNN
F 2 "~" H 1575 6725 60  0000 C CNN
F 3 "~" H 1575 6725 60  0000 C CNN
	1    1575 6725
	-1   0    0    -1  
$EndComp
$Comp
L R-SMALL R213
U 1 1 6044AECD
P 1575 6975
F 0 "R213" H 1575 7075 40  0000 C CNN
F 1 "47k" H 1575 6975 40  0000 C CNN
F 2 "~" H 1575 6975 60  0000 C CNN
F 3 "~" H 1575 6975 60  0000 C CNN
	1    1575 6975
	-1   0    0    -1  
$EndComp
$Comp
L CSMALL C204
U 1 1 6044AED3
P 1575 7200
F 0 "C204" V 1650 7075 40  0000 L CNN
F 1 "100pF" V 1450 7125 40  0000 L CNN
F 2 "~" H 1575 7200 60  0000 C CNN
F 3 "~" H 1575 7200 60  0000 C CNN
	1    1575 7200
	0    1    -1   0   
$EndComp
$Comp
L DIODESCH D204
U 1 1 6044AED9
P 1625 6400
F 0 "D204" H 1625 6500 40  0000 C CNN
F 1 "RB751V40" H 1625 6300 40  0000 C CNN
F 2 "~" H 1625 6400 60  0000 C CNN
F 3 "~" H 1625 6400 60  0000 C CNN
	1    1625 6400
	1    0    0    1   
$EndComp
$Comp
L GND #PWR064
U 1 1 6044AEEA
P 2025 7050
F 0 "#PWR064" H 2025 7050 30  0001 C CNN
F 1 "GND" H 2025 6980 30  0001 C CNN
F 2 "" H 2025 7050 60  0000 C CNN
F 3 "" H 2025 7050 60  0000 C CNN
	1    2025 7050
	-1   0    0    -1  
$EndComp
$Comp
L +3.3V #PWR065
U 1 1 6044AEF2
P 1950 6300
F 0 "#PWR065" H 1950 6260 30  0001 C CNN
F 1 "+3.3V" H 1950 6410 30  0000 C CNN
F 2 "" H 1950 6300 60  0000 C CNN
F 3 "" H 1950 6300 60  0000 C CNN
	1    1950 6300
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR066
U 1 1 6044B01B
P 2850 5075
F 0 "#PWR066" H 2850 5075 30  0001 C CNN
F 1 "GND" H 2850 5005 30  0001 C CNN
F 2 "" H 2850 5075 60  0000 C CNN
F 3 "" H 2850 5075 60  0000 C CNN
	1    2850 5075
	1    0    0    -1  
$EndComp
Text HLabel 1000 2950 0    40   Input ~ 0
A1
Text HLabel 1000 4000 0    40   Input ~ 0
A2
Text HLabel 1000 5300 0    40   Input ~ 0
A3
Text HLabel 1000 6600 0    40   Input ~ 0
A4
Text Label 2175 2825 0    40   ~ 0
A1ext
Text Label 2175 4125 0    40   ~ 0
A2ext
Text Label 2175 5425 0    40   ~ 0
A3ext
Text Label 2175 6725 0    40   ~ 0
A4ext
$Comp
L N5110 J203
U 1 1 6044CF65
P 10075 1050
F 0 "J203" H 9575 1150 60  0000 C CNN
F 1 "N5110" H 10000 950 60  0000 C CNN
F 2 "" H 10075 1050 60  0000 C CNN
F 3 "" H 10075 1050 60  0000 C CNN
	1    10075 1050
	1    0    0    1   
$EndComp
$Comp
L GND #PWR067
U 1 1 6044CF6B
P 10425 1500
F 0 "#PWR067" H 10425 1500 30  0001 C CNN
F 1 "GND" H 10425 1430 30  0001 C CNN
F 2 "" H 10425 1500 60  0000 C CNN
F 3 "" H 10425 1500 60  0000 C CNN
	1    10425 1500
	-1   0    0    -1  
$EndComp
$Comp
L +3.3V #PWR068
U 1 1 6044CF71
P 10575 875
F 0 "#PWR068" H 10575 835 30  0001 C CNN
F 1 "+3.3V" H 10575 985 30  0000 C CNN
F 2 "" H 10575 875 60  0001 C CNN
F 3 "" H 10575 875 60  0001 C CNN
	1    10575 875 
	-1   0    0    -1  
$EndComp
Text Label 9450 1850 2    40   ~ 0
SCK
Text Label 9450 1750 2    40   ~ 0
MOSI
Text Label 9450 1650 2    40   ~ 0
DC-51
Text Label 9450 1550 2    40   ~ 0
CE-51
Text Label 9700 1450 2    40   ~ 0
RST-51
Text Notes 10625 2075 2    60   ~ 12
Optional Display
NoConn ~ 10325 1350
$Comp
L R-SMALL R216
U 1 1 6044CF8F
P 9425 1150
F 0 "R216" H 9425 1025 60  0000 C CNN
F 1 "1k5" H 9425 1150 40  0000 C CNN
F 2 "~" H 9425 1150 60  0000 C CNN
F 3 "~" H 9425 1150 60  0000 C CNN
	1    9425 1150
	0    1    -1   0   
$EndComp
$Comp
L +3.3V #PWR069
U 1 1 6044CF96
P 9425 1000
F 0 "#PWR069" H 9425 960 30  0001 C CNN
F 1 "+3.3V" H 9425 1110 30  0000 C CNN
F 2 "" H 9425 1000 60  0000 C CNN
F 3 "" H 9425 1000 60  0000 C CNN
	1    9425 1000
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2675 1325 2575 1325
Wire Wire Line
	2575 1325 2575 1600
Wire Wire Line
	2225 1500 4075 1500
Wire Wire Line
	3375 1500 3375 1325
Connection ~ 2575 1500
Wire Wire Line
	4075 1500 4075 1325
Connection ~ 3375 1500
Wire Wire Line
	4825 1000 4825 925 
Wire Wire Line
	2650 1000 4825 1000
Wire Wire Line
	3175 1000 3175 1225
Wire Wire Line
	3875 1225 3875 1000
Connection ~ 3875 1000
Wire Wire Line
	4575 1225 4575 1000
Connection ~ 4575 1000
Wire Wire Line
	3175 1325 3275 1325
Wire Wire Line
	3275 1325 3275 1225
Wire Wire Line
	3275 1225 3375 1225
Wire Wire Line
	3875 1325 3975 1325
Wire Wire Line
	3975 1325 3975 1225
Wire Wire Line
	3975 1225 4075 1225
Wire Wire Line
	2225 1450 2225 1500
Wire Wire Line
	2225 1000 2225 1250
Wire Wire Line
	2225 1225 2675 1225
Connection ~ 3175 1000
Wire Wire Line
	2350 1000 2225 1000
Connection ~ 2225 1225
Wire Wire Line
	2075 1350 1650 1350
Wire Wire Line
	1725 2825 2625 2825
Wire Wire Line
	1425 2825 1325 2825
Wire Wire Line
	1325 2500 1325 3300
Wire Wire Line
	1325 3300 1475 3300
Wire Wire Line
	1425 3075 1325 3075
Connection ~ 1325 3075
Wire Wire Line
	1675 3300 1825 3300
Wire Wire Line
	1825 3300 1825 3075
Wire Wire Line
	1725 3075 2025 3075
Wire Wire Line
	2025 3075 2025 3150
Connection ~ 1825 3075
Wire Wire Line
	1425 2500 1325 2500
Connection ~ 1325 2825
Wire Wire Line
	1950 2400 1950 2500
Wire Wire Line
	1950 2500 1825 2500
Wire Wire Line
	1725 4125 2425 4125
Wire Wire Line
	1325 4125 1425 4125
Wire Wire Line
	1325 3800 1325 4600
Wire Wire Line
	1325 4600 1475 4600
Wire Wire Line
	1425 4375 1325 4375
Connection ~ 1325 4375
Wire Wire Line
	1675 4600 1825 4600
Wire Wire Line
	1825 4600 1825 4375
Wire Wire Line
	1725 4375 2025 4375
Wire Wire Line
	2025 4375 2025 4450
Connection ~ 1825 4375
Wire Wire Line
	1425 3800 1325 3800
Connection ~ 1325 4125
Wire Wire Line
	1950 3700 1950 3800
Wire Wire Line
	1950 3800 1825 3800
Wire Wire Line
	1725 5425 2425 5425
Wire Wire Line
	1325 5425 1425 5425
Wire Wire Line
	1325 5100 1325 5900
Wire Wire Line
	1325 5900 1475 5900
Wire Wire Line
	1425 5675 1325 5675
Connection ~ 1325 5675
Wire Wire Line
	1675 5900 1825 5900
Wire Wire Line
	1825 5900 1825 5675
Wire Wire Line
	1725 5675 2025 5675
Wire Wire Line
	2025 5675 2025 5750
Connection ~ 1825 5675
Wire Wire Line
	1425 5100 1325 5100
Connection ~ 1325 5425
Wire Wire Line
	1950 5000 1950 5100
Wire Wire Line
	1950 5100 1825 5100
Wire Wire Line
	1725 6725 2625 6725
Wire Wire Line
	1325 6725 1425 6725
Wire Wire Line
	1325 6400 1325 7200
Wire Wire Line
	1325 7200 1475 7200
Wire Wire Line
	1425 6975 1325 6975
Connection ~ 1325 6975
Wire Wire Line
	1675 7200 1825 7200
Wire Wire Line
	1825 7200 1825 6975
Wire Wire Line
	1725 6975 2025 6975
Wire Wire Line
	2025 6975 2025 7050
Connection ~ 1825 6975
Wire Wire Line
	1425 6400 1325 6400
Connection ~ 1325 6725
Wire Wire Line
	1950 6300 1950 6400
Wire Wire Line
	1950 6400 1825 6400
Wire Wire Line
	1325 2950 1000 2950
Connection ~ 1325 2950
Wire Wire Line
	1325 4000 1000 4000
Wire Wire Line
	1325 5300 1000 5300
Wire Wire Line
	1325 6600 1000 6600
Connection ~ 1325 6600
Connection ~ 1325 5300
Connection ~ 1325 4000
Wire Wire Line
	3000 4400 2850 4400
Wire Wire Line
	2850 4400 2850 5075
Wire Wire Line
	3000 4900 2850 4900
Connection ~ 2850 4900
Wire Wire Line
	3000 4500 2625 4500
Wire Wire Line
	2625 4500 2625 2825
Wire Wire Line
	3000 4600 2425 4600
Wire Wire Line
	2425 4600 2425 4125
Wire Wire Line
	3000 4700 2425 4700
Wire Wire Line
	2425 4700 2425 5425
Wire Wire Line
	3000 4800 2625 4800
Wire Wire Line
	2625 4800 2625 6725
Wire Wire Line
	10425 1350 10425 1500
Wire Wire Line
	10225 1350 10225 1675
Wire Wire Line
	10225 1675 10575 1675
Wire Wire Line
	10575 1675 10575 875 
Wire Wire Line
	10125 1350 10125 1850
Wire Wire Line
	10125 1850 9150 1850
Wire Wire Line
	10025 1350 10025 1750
Wire Wire Line
	10025 1750 9150 1750
Wire Wire Line
	9925 1350 9925 1650
Wire Wire Line
	9925 1650 9150 1650
Wire Wire Line
	9825 1350 9825 1550
Wire Wire Line
	9825 1550 9150 1550
Wire Wire Line
	9725 1350 9725 1450
Wire Notes Line
	10725 700  8800 700 
Wire Notes Line
	8800 700  8800 2100
Wire Notes Line
	8800 2100 10725 2100
Wire Notes Line
	10725 2100 10725 700 
Wire Wire Line
	9425 1300 9425 1450
Wire Wire Line
	9425 1450 9725 1450
Text HLabel 9150 1550 0    40   Input ~ 0
CE51
Text HLabel 9150 1650 0    40   Input ~ 0
DC51
Text HLabel 9150 1750 0    40   Input ~ 0
MOSI51
Text HLabel 9150 1850 0    40   Input ~ 0
SCK51
$Comp
L BUZZER BZ201
U 1 1 60450928
P 10225 2725
F 0 "BZ201" H 10225 2775 60  0000 C CNN
F 1 "BUZZER" H 10225 2675 60  0000 C CNN
F 2 "~" H 10225 2725 60  0000 C CNN
F 3 "~" H 10225 2725 60  0000 C CNN
	1    10225 2725
	1    0    0    -1  
$EndComp
$Comp
L R-SMALL R215
U 1 1 6045093F
P 9575 2675
F 0 "R215" H 9575 2775 40  0000 C CNN
F 1 "100R" H 9575 2675 40  0000 C CNN
F 2 "~" H 9575 2675 60  0000 C CNN
F 3 "~" H 9575 2675 60  0000 C CNN
	1    9575 2675
	-1   0    0    -1  
$EndComp
Wire Wire Line
	9725 2675 9875 2675
Wire Wire Line
	9875 2775 9750 2775
Wire Wire Line
	9750 2775 9750 2975
$Comp
L GND #PWR070
U 1 1 60450A6C
P 9750 2975
F 0 "#PWR070" H 9750 2975 30  0001 C CNN
F 1 "GND" H 9750 2905 30  0001 C CNN
F 2 "" H 9750 2975 60  0000 C CNN
F 3 "" H 9750 2975 60  0000 C CNN
	1    9750 2975
	1    0    0    -1  
$EndComp
Wire Wire Line
	9250 2675 9425 2675
Text HLabel 9250 2675 0    40   Input ~ 0
BUZZ
$Comp
L RELAY-SPST-G6DN RL201
U 1 1 60451D57
P 5600 2650
F 0 "RL201" H 5600 3000 40  0000 C CNN
F 1 "RELAY-SPST-G6DN" V 5750 2650 40  0000 C CNN
F 2 "~" H 5600 2550 60  0000 C CNN
F 3 "~" H 5600 2550 60  0000 C CNN
	1    5600 2650
	1    0    0    -1  
$EndComp
$Comp
L CONN-3.5MM J202
U 1 1 60451D70
P 5600 3200
F 0 "J202" H 5800 3200 40  0000 C CNN
F 1 "thermostat" H 5600 3050 40  0000 C CNN
F 2 "conn-3.5mm" H 5600 3000 20  0001 C CNN
F 3 "~" H 5600 3200 60  0000 C CNN
	1    5600 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5400 3150 5300 3150
Wire Wire Line
	5300 3150 5300 2850
Wire Wire Line
	5300 2850 5450 2850
Wire Wire Line
	5400 3250 5250 3250
Wire Wire Line
	5250 3250 5250 2750
Wire Wire Line
	5250 2750 5450 2750
Wire Wire Line
	4825 2600 5450 2600
$Comp
L +5V #PWR071
U 1 1 60451F2F
P 5275 2075
F 0 "#PWR071" H 5275 2165 20  0001 C CNN
F 1 "+5V" H 5275 2165 30  0000 C CNN
F 2 "" H 5275 2075 60  0000 C CNN
F 3 "" H 5275 2075 60  0000 C CNN
	1    5275 2075
	1    0    0    -1  
$EndComp
$Comp
L NMOS Q202
U 1 1 60451F3C
P 4775 2775
F 0 "Q202" H 4875 2775 40  0000 C CNN
F 1 "2N27002" H 4575 2925 40  0000 C CNN
F 2 "~" H 4775 2775 60  0000 C CNN
F 3 "~" H 4775 2775 60  0000 C CNN
	1    4775 2775
	1    0    0    -1  
$EndComp
Wire Wire Line
	4825 2875 4825 3100
Wire Wire Line
	4250 2775 4675 2775
$Comp
L GND #PWR072
U 1 1 60451F47
P 4825 3100
F 0 "#PWR072" H 4825 3100 30  0001 C CNN
F 1 "GND" H 4825 3030 30  0001 C CNN
F 2 "" H 4825 3100 60  0000 C CNN
F 3 "" H 4825 3100 60  0000 C CNN
	1    4825 3100
	1    0    0    -1  
$EndComp
$Comp
L R-SMALL R214
U 1 1 60451F54
P 4575 2950
F 0 "R214" H 4575 3050 40  0000 C CNN
F 1 "1k" H 4575 2950 40  0000 C CNN
F 2 "~" H 4575 2950 60  0000 C CNN
F 3 "~" H 4575 2950 60  0000 C CNN
	1    4575 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	4725 2950 4825 2950
Connection ~ 4825 2950
Wire Wire Line
	4425 2950 4350 2950
Wire Wire Line
	4350 2950 4350 2775
Connection ~ 4350 2775
$Comp
L DIODE D205
U 1 1 604520BF
P 5075 2375
F 0 "D205" H 5075 2475 40  0000 C CNN
F 1 "1N4148" H 5075 2275 40  0000 C CNN
F 2 "~" H 5075 2375 60  0000 C CNN
F 3 "~" H 5075 2375 60  0000 C CNN
	1    5075 2375
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5075 2600 5075 2575
Wire Wire Line
	4825 2675 4825 2600
Connection ~ 5075 2600
Wire Wire Line
	5075 2175 5075 2125
Wire Wire Line
	5075 2125 5275 2125
Connection ~ 5275 2125
Wire Wire Line
	5275 2075 5275 2450
Wire Wire Line
	5275 2450 5450 2450
Text HLabel 4250 2775 0    40   Input ~ 0
RELAY
$Comp
L STRIP-3 U204
U 1 1 604546AF
P 8350 1375
F 0 "U204" V 8100 1400 40  0000 C CNN
F 1 "MCP9701A" H 8350 1475 40  0000 C CNN
F 2 "~" H 8300 1375 60  0000 C CNN
F 3 "~" H 8300 1375 60  0000 C CNN
	1    8350 1375
	0    1    1    0   
$EndComp
$Comp
L +3.3V #PWR073
U 1 1 604546B5
P 8100 1150
F 0 "#PWR073" H 8100 1110 30  0001 C CNN
F 1 "+3.3V" H 8100 1260 30  0000 C CNN
F 2 "" H 8100 1150 60  0000 C CNN
F 3 "" H 8100 1150 60  0000 C CNN
	1    8100 1150
	1    0    0    -1  
$EndComp
$Comp
L CSMALL C206
U 1 1 604546BC
P 7975 1525
F 0 "C206" H 8150 1425 40  0000 L CNN
F 1 "100n" H 8025 1625 40  0000 L CNN
F 2 "~" H 7975 1525 60  0000 C CNN
F 3 "~" H 7975 1525 60  0000 C CNN
	1    7975 1525
	1    0    0    -1  
$EndComp
$Comp
L CSMALL C205
U 1 1 604546C2
P 7800 1525
F 0 "C205" H 7625 1450 40  0000 L CNN
F 1 "100n" H 7600 1600 40  0000 L CNN
F 2 "~" H 7800 1525 60  0000 C CNN
F 3 "~" H 7800 1525 60  0000 C CNN
	1    7800 1525
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR074
U 1 1 604546CE
P 7150 1725
F 0 "#PWR074" H 7150 1725 30  0001 C CNN
F 1 "GND" H 7150 1655 30  0001 C CNN
F 2 "" H 7150 1725 60  0000 C CNN
F 3 "" H 7150 1725 60  0000 C CNN
	1    7150 1725
	1    0    0    -1  
$EndComp
Wire Wire Line
	8100 1150 8100 1275
Wire Wire Line
	7800 1275 8300 1275
Wire Wire Line
	7175 1375 8300 1375
Wire Wire Line
	8300 1475 8100 1475
Wire Wire Line
	8100 1475 8100 1700
Wire Notes Line
	8675 850  8675 1925
Wire Notes Line
	8675 850  6800 850 
Wire Notes Line
	6800 850  6800 1925
Wire Notes Line
	6800 1925 8675 1925
Text Notes 8600 1875 2    60   ~ 12
Local thermometer
Wire Wire Line
	8100 1700 7150 1700
Wire Wire Line
	7150 1700 7150 1725
Connection ~ 7800 1700
Wire Wire Line
	7975 1375 7975 1425
Connection ~ 7975 1375
Wire Wire Line
	7975 1625 7975 1700
Connection ~ 7975 1700
Wire Wire Line
	7800 1275 7800 1425
Connection ~ 8100 1275
Wire Wire Line
	7800 1625 7800 1700
Text HLabel 7175 1375 0    40   Input ~ 0
THERMO
$Comp
L R-SMALL R203
U 1 1 60457187
P 7450 1525
F 0 "R203" H 7450 1625 40  0000 C CNN
F 1 "47k" H 7450 1525 40  0000 C CNN
F 2 "~" H 7450 1525 60  0000 C CNN
F 3 "~" H 7450 1525 60  0000 C CNN
	1    7450 1525
	0    -1   1    0   
$EndComp
Connection ~ 7450 1375
Wire Wire Line
	7450 1675 7450 1700
Connection ~ 7450 1700
$EndSCHEMATC
