#!/usr/bin/wish

# global graphic defaults
set g(cwid) 800
set g(cstep) 10; # 10 pixels per calculation time
set g(sephei) 5; # 5 pixels per separator
set g(colors) {
    "#468" red green blue cyan magenta \#cc0 orange gray50 gray30 "#c00"
}

# global sched default
set g(nt:max) 10
set g(nt:min)  1
set g(nt)      1

set g(p:max) 20
set g(p:min)  1
set g(c:max) 20
set g(c:min)  0

# the other global is "ss" (system status), built at runtime

# main items
pack [frame .t] -side left -expand y -fill both
pack [canvas .c -width $g(cwid) -height 0 -bg white] \
    -expand y -fill both -anchor nw



# create all the frames, we'll pack them later (task 0 is the server)
for {set i 0} {$i<=$g(nt:max)} {incr i} {
    set g(col:$i) [lindex $g(colors) $i]
    set f [frame .t.t$i]
    pack [frame $f.sep -bg gray50 -height 5] -expand true -fill x
    pack [label $f.l -text "Task $i" -width 10 -bg white -fg $g(col:$i)] \
	-side left -expand true -fill y
    
    # period and calculation time
    set g(p:$i) $g(p:max)
    set g(c:$i) $g(c:min)
    foreach n {p c} {
	pack [frame $f.$n] -side top
	pack [label $f.$n.lab -textvar g($n:$i) -width 4] -side left -anchor e
	pack [scale $f.$n.sc -from $g($n:min) -to $g($n:max) -var g($n:$i) \
		  -orient h -showvalue false] -expand true -fill x
    }
}

# create the other widgets, in frame number "task-control"
pack [set topf [frame .t.tc]]	-expand true -fill x

pack [scale $topf.ntask -orient h -from 1 -to 10 -variable g(nt)] \
	-expand true -fill x
pack [set f [frame $topf.sch]] -expand true -fill x
foreach n {RM EDF} {
    set m [string tolower $n]
    if ![info exists g(sched)] {set g(sched) $m}
    pack [radiobutton $f.$m -text $n -variable g(sched) -value $m] \
	-side left -expand true -fill x
}
pack [set f [frame $topf.ser]] -expand true -fill x
foreach n {none BS PS DS} {
    set m [string tolower $n]
    if ![info exists g(server)] {set g(server) $m}
    pack [radiobutton $f.$m -text $n -variable g(server) -value $m] \
	-side left -expand true -fill x
}
set ss(u) "U = 0"
pack [label $topf.u -textv ss(u) -width 10]

# the server
.t.t0.l config -text "Server"
pack .t.t0 -after .t.tc

# pack first, calc its height
pack .t.t1 -after .t.t0
update
set g(taskhei) [winfo height .t.t1]
set g(taskhei0) [expr [winfo height .t.tc] + [winfo height .t.t0]]

# and this is the recalculate engine. name1 is always "g", op is always w
proc recalc {name1 name2 op} {
    global g ss
    if ![string compare $name2 "nt"] {
	# pack/unpack windows
	for {set i 1; set prev 0} {$i<=$g(nt:max)} {incr prev; incr i} {
	    if $i>$g(nt) {
		pack unpack .t.t$i
	    } else {
		pack .t.t$i -after .t.t$prev
	    }
	}
    }
    # run the scheduler: first delete all items and zero all computation time
    .c delete sched dead
    set u 0.0
    for {set i 1} {$i <= $g(nt)} {incr i} {
	set ss(c:$i) 0
	set u [expr $u + double($g(c:$i))/$g(p:$i)]
    }
    set u [expr $u + [server_$g(server)_lub]]
    set ss(current) 0
    array unset ss aper:*; # clean internal status of the server
    set ss(c:0) 0
    set ss(aper:pending) 0
    set ss(u) [format "U = %.3f" $u]

    # for each time quantum
    for {set x [set n 0]} {$x <= $g(cwid)} {incr x $g(cstep); incr n} {
	# update task state (not the server)
	for {set i 1} {$i <= $g(nt)} {incr i} {
	    # release time?
	    if {!($n % $g(p:$i))} {
		if $ss(c:$i) {
		    # can't meet deadline
		    .c create line $x $g(y:$i) $x [expr $g(y:$i)+20] \
			-width 3 -fill black -arrow first -tag sched
		    .c create line $x $g(y:-1) $x [expr $g(y:-1)+20] \
			-width 3 -fill $g(col:$i) -arrow first -tag sched
		}
		# update calc time and absolute deadline (draw it too)
		incr ss(c:$i) $g(c:$i)
		set ss(dead:$i) [expr $n+$g(p:$i)]
		.c create line $x $g(y:$i) $x [expr $g(y:$i)-20] -tag sched \
		    -fill black -width 1 -arrow first -tag dead
	    }
	}
	# aperiodic activation
	if [info exists g(aper:$n)] {
	    .c create line $x $g(y:0) $x [expr $g(y:0)-$g(aper:$n)*$g(cstep)] \
		-fill black -width 1 -arrow last -tag dead
	    incr ss(aper:pending) $g(aper:$n)
	}

	# call server per-computation
	server_$g(server)_pre $n
	# call scheduler: only arg is current time (and system-status)
	set ss(current) [sched_$g(sched) $n]
	if 0 {puts "time $n: list $ss(list) -- current $ss(current)"}
	# call server again, use $i as easy cache for current
	set i [set ss(current) [server_$g(server)_post $n $x]]

	if $i>0 {
	    # now update computation time
	    incr ss(c:$i) -1
	    # and draw items
	    foreach name "-1 $i" {
		.c create rectangle $x $g(y:$name) \
		    [expr $x+$g(cstep)] [expr $g(y:$name)-$g(cstep)] \
		    -fill $g(col:$i) -outline $g(col:$i) -tag sched
	    }
	    # if the current task is done, it's not current any more
	    if !$ss(c:$i) {set ss(current) -1}
	}
    }
    # leave the grid and deadlines over everything
    .c raise grid
    .c raise dead
}




# create the grid on-canvas (task 0 is the server, -1 is general)
for {set i -1} {$i<=$g(nt:max)} {incr i} {
    set y [set g(y:$i) [expr int($g(taskhei0) + ($i-.5)*$g(taskhei))]]
    if $i==-1 {set y [set g(y:$i) [expr $g(taskhei)/2]]}
    .c create line 0 $y $g(cwid) $y -fill black -width 3 -tag grid
    for {set x [set n 0]} {$x <= $g(cwid)} {incr x $g(cstep); incr n} {
	set d 10; if $n%10 {set d 7}; if $n%5 {set d 4}
	.c create line $x [expr $y+$d] $x [expr $y-$d] -fill black -tag grid
    }
}


if ![info exists env(DEBUG)] {
    # set a trace on g, to recalculate stuff
    trace variable g w recalc
    after 200 "set g(nt) $g(nt)"; # first run
} else {
    # debugging aid, as a trace has too few error messages
    proc repeat {} {
	recalc g nt w
	after 200 repeat
    }
    after 200 repeat
}


#### canvas binding for aperiodic tasks (B1 increases, B2 decreases)
bind .c <1> {press 1 %x}
bind .c <3> {press -1 %x}

proc press {amount where} {
    global g
    set x [expr $where/$g(cstep)]
    if ![info exists g(aper:$x)] {set g(aper:$x) 0}
    if ![incr g(aper:$x) $amount] {unset g(aper:$x)}
}


#### rm scheduler
proc sched_rm {n} {
    global g ss
    # build a list of active tasks (it can't be empty)
    set l "{999999 -1}"
    for {set i 0} {$i <= $g(nt)} {incr i} {
	if $ss(c:$i) {
	    lappend l "$g(p:$i) $i"
	}
    }
    # sort it according to relative deadline, save it for the server
    set ss(list) [lsort -integer -index 0 $l]
    return [lindex [lindex $ss(list) 0] 1]
}

#### edf scheduler
proc sched_edf {n} {
    global g ss
    # build a list of active tasks (it can't be empty)
    set l "{999999 -1}"
    for {set i 0} {$i <= $g(nt)} {incr i} {
	if $ss(c:$i) {
	    lappend l "$ss(dead:$i) $i"
	}
    }
    # sort it according to absolute deadline, save it for the server
    set ss(list) [lsort -integer -index 0 $l]
    # prefer no preemption
    set res [lindex $ss(list) 0]
    set wins [lindex $res 0]
    if {$ss(current)>0 && $ss(dead:$ss(current))==$wins} {
	return $ss(current)
    }
    return [lindex $res 1]
}


######## servers (pre is scheduling, post is drawing)
proc server_none_lub {} {
    return 0.0
}
proc server_none_pre {n} {
    global ss
    set ss(c:0) 0; # nothing to do
}

proc server_none_post {n x} {
    global ss
    return $ss(current)
}


#### background
proc server_bs_lub {} {
    return 0.0
}
proc server_bs_pre {n} {
    global ss
    set ss(c:0) 0; # no deadline
}

proc server_bs_post {n x} {
    global g ss
    if $ss(current)>0 {return $ss(current)}
    # draw our stuff
    set item [.c create rectangle $x $g(y:0) \
		  [expr $x+$g(cstep)] [expr $g(y:0)-$g(cstep)] \
		  -outline $g(col:0) -tag sched]
    if $ss(aper:pending) {
	.c itemco $item -fill $g(col:0)
	incr ss(aper:pending) -1
    }
    return 0
}

#### polling server
proc server_ps_lub {} {
    global g
    return [expr double($g(c:0))/$g(p:0)]
}

proc server_ps_pre {n} {
    global g ss
    if !$n {puts "time 0, pending $ss(aper:pending)"}
    if $n%$g(p:0) return;
    # activation time: only if pending we are active
    if $ss(aper:pending) {
	set ss(c:0) $g(c:0)
	set ss(dead:0) [expr $n+$g(p:0)]
    }
}

proc server_ps_post {n x} {
    global g ss
    set xx [expr $x+$g(cstep)]

    if !$n {puts "iteration 0: current $ss(current), list $ss(list)"}

    if !($n%$g(p:0)) {
	# activation line
	.c create line $x $g(y:0) $x [expr $g(y:0)-$g(c:0)*$g(cstep)] \
	    -width 3 -fill red -tag sched
    }
    
    if $ss(current) {
	# keep your charge
	if $ss(c:0) {
	    .c create line $x [expr $g(y:0)-$ss(c:0)*$g(cstep)] \
		$xx [expr $g(y:0)-$ss(c:0)*$g(cstep)] \
		-width 3 -fill red -tag sched
	}
	return $ss(current)
    }

    # update our status; if no more pending we are done
    set prevc $ss(c:0)
    incr ss(c:0) -1
    incr ss(aper:pending) -1
    if !$ss(aper:pending) {set ss(c:0) 0}

    # draw our stuff: rect, load decreasing, possibly zeroing
    .c create rectangle $x $g(y:0) $xx [expr $g(y:0)-$g(cstep)] \
	-outline $g(col:0) -fill $g(col:0) -tag sched
    .c create line $x [expr $g(y:0)-$prevc*$g(cstep)] \
	$xx [expr $g(y:0)-($prevc-1)*$g(cstep)] \
	-width 3 -fill red -tag sched
    if !$ss(c:0) {
    .c create line [expr $x+$g(cstep)] [expr $g(y:0)-($prevc-1)*$g(cstep)] \
	$xx $g(y:0) \
	-width 3 -fill red -tag sched
    }
    return 0; # we did run
}

#### deferrable server
proc server_ds_lub {} {
    global g
    return [expr double($g(c:0))/$g(p:0)]
}

proc server_ds_pre {n} {
    global g ss
    if $n%$g(p:0) return;
    # activation time: always replenish
    set ss(c:0) $g(c:0)
    set ss(dead:0) [expr $n+$g(p:0)]
}

proc server_ds_post {n x} {
    global g ss
    set xx [expr $x+$g(cstep)]

    if !($n%$g(p:0)) {
	# activation line
	.c create line $x $g(y:0) $x [expr $g(y:0)-$g(c:0)*$g(cstep)] \
	    -width 3 -fill red -tag sched
    }
    
    if $ss(current) {
	# keep your charge
	if $ss(c:0) {
	    .c create line $x [expr $g(y:0)-$ss(c:0)*$g(cstep)] \
		$xx [expr $g(y:0)-$ss(c:0)*$g(cstep)] \
		-width 3 -fill red -tag sched
	}
	return $ss(current)
    }

    # update our status: if pending we run
    set prevc $ss(c:0)
    if $ss(aper:pending) {
	set didrun 1
	set currc [incr ss(c:0) -1]
	incr ss(aper:pending) -1
	# draw rect
	.c create rectangle $x $g(y:0) $xx [expr $g(y:0)-$g(cstep)] \
	    -outline $g(col:0) -fill $g(col:0) -tag sched
    } else {
	set didrun 0
	set currc $prevc
    }

    # draw our lines: load decreasing, possibly zeroing
    .c create line $x [expr $g(y:0)-$prevc*$g(cstep)] \
	$xx [expr $g(y:0)-$currc*$g(cstep)] \
	-width 3 -fill red -tag sched
    if $didrun {return 0}
    # we didn't run: select the next task
    return [lindex [lindex $ss(list) 1] 1]
}

