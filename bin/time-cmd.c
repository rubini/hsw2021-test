#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <errno.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <sys/types.h>


int main(int argc, char **argv)
{
    int pid, status;
    struct timeval tv1, tv2;

    gettimeofday(&tv1, NULL);
    switch (pid=fork()) {
    case -1:
	fprintf(stderr, "fork(): %s\n", strerror(errno));
	exit(1);
    case 0: /* child */
	/* put stderr to stdout */
	close(2); dup(1);
	execvp(argv[1], argv+1);
	fprintf(stderr, "exec(): %s\n", strerror(errno));
	exit(1);
    }

    wait(&status);
    gettimeofday(&tv2, NULL);
    tv2.tv_sec -= tv1.tv_sec;
    if (tv2.tv_usec >= tv1.tv_usec) tv2.tv_usec -= tv1.tv_usec;
    else tv2.tv_sec--, tv2.tv_usec = tv2.tv_usec + 1000*1000 - tv1.tv_usec;
    fprintf(stderr, "%i.%06i sec\n", (int)tv2.tv_sec, (int)tv2.tv_usec);
    if (WIFEXITED(status))
	fprintf(stderr, "(\"%s\" pid %i: exit %i)\n",
		argv[1], pid, WEXITSTATUS(status));
    else if (WIFSIGNALED(status))
	fprintf(stderr, "(\"%s\"pid %i: signal %i)\n",
		argv[1], pid, WTERMSIG(status));
    exit(0);
}
